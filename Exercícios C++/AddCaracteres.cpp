#include <iostream>
#include <string.h>

using namespace std;

void addCaracteres (char [], char, int);

int main () {
    char nome[] = "Lcas Bittencourt";
    addCaracteres(nome, 'u', 1);
    cout << nome << endl;
    return 0;
}

void addCaracteres (char txt[], char ch, int pos) {
    int tam = strlen(txt);
    char str[tam + 1];
    int qtdAdded = 0;
    for (int i = 0; i < tam + 1; i++)
        if (i == pos) {
            str[i] = ch;
            qtdAdded++;
        }
        else str[i] = txt[i - qtdAdded];
    for (int i = 0; i < tam + 1; i++)
        txt[i] = str[i];
    txt[tam + 1] = '\0';
}