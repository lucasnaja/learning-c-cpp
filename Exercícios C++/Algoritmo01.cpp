#include <iostream>

int main(void)
{
    /* --> 6
        1
        22
        333
        4444
        55555
        666666
    */

    int num;
    std::cout << "Digite o número: ";
    std::cin >> num;

    for (int i = 1; i <= num; i++)
    {
        for (int j = 0; j < i; j++)
            std::cout << i;
        std::cout << std::endl;
    }

    return 0;
}
