#include <iostream>

int main(void)
{
    /* --> 6
        1
        212
        32123
        4321234
        543212345
        65432123456
    */

    int num;
    std::cout << "Digite o número: ";
    std::cin >> num;

    for (int i = 1; i <= num; i++)
    {
        std::cout << i;
        for (int j = i - 1; j > 1; j--)
            std::cout << j;
        for (int j = 1; j <= i && i != 1; j++)
            std::cout << j;
        std::cout << std::endl;
    }

    return 0;
}