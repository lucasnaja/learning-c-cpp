#include <iostream>

int main(void)
{
    /*
        11111
        2222
        333
        44
        5
    */

    int qtd = 5;

    for (int i = 0; i < qtd; i++)
    {
        for (int j = qtd - i; j > 0; j--)
            std::cout << i + 1;
        std::cout << std::endl;
    }

    return 0;
}
