#include <iostream>

int main(void)
{
    /*
        A
        A A
        A A A
        A A A A
    */

    for (int i = 1; i <= 5; i++)
    {
        for (int j = 0; j < i; j++)
            std::cout << "A ";
        std::cout << std::endl;
    }
    return 0;
}
