#include <iostream>
using namespace std;
#define MAX 5

// Compilar: g++ -std=c++1z nome.cpp -o nome

class Int
{
    int y[MAX] = {1, 2, 3, 4, 5};

  public:
    Int() {}
    Int(int y[MAX])
    {
        for (int i = 0; i < MAX; i++)
            Int::y[i] = y[i];
    }

    ~Int() {}

    int &operator[](int valor)
    {
        static int x = -1;
        if (valor >= 0 && valor < MAX)
        {
            cout << y[valor] << endl;
            return y[valor];
        }
        else
        {
            cout << "ERRO: ULTRAPASSOU OS LIMITES DA MATRIZ!\n";
            return x;
        }
    }

    void print()
    {
        for (int i = 0; i < MAX; i++)
            cout << y[i] << (i == MAX - 1 ? "\n" : ", ");
    }
};

int main(void)
{
    int max[MAX] = {1, 2, 3, 4, 5};
    Int a = max, b = max, c = max, d, e, f;
    e.print();
    a[10];
    a[4];
    return 0;
}