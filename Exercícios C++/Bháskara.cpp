#include <iostream>
#include <math.h>

int main(void)
{
    long a, b, c;
    long double delta, x1, x2;

    std::cout << "-> Digite o valor de A [0 = Cancelar]: ";
    std::cin >> a;

    if (a == 0)
        return 0;

    std::cout << "-> Digite o valor de B: ";
    std::cin >> b;
    std::cout << "-> Digite o valor de C: ";
    std::cin >> c;

    std::cout << "\nΔ = B² - 4 * A * C\n";
    std::cout << "Δ = " << b << "² - 4 * " << a << " * " << c << '\n';

    char sinal;
    if (4 * a * c < 0)
        sinal = '+';
    else
        sinal = '-';

    std::cout << "Δ = " << b * b << ' ' << sinal << ' ' << (4 * a * c < 0 ? -4 * a * c : 4 * a * c) << '\n';
    std::cout << "Δ = " << b * b - 4 * a * c << '\n';

    delta = b * b - 4 * a * c;
    if (delta < 0)
    {
        std::cout << "\n-> Para Delta menor que 0, não existem raízes reais.\n";
        return 0;
    }
    else if (delta == 0)
        std::cout << "\n-> Para Delta igual a 0, existem duas raízes reais iguais.\n\n";
    else
        std::cout << "\n-> Para Delta maior que 0, existem duas raízes reais distintas.\n\n";

    std::cout << "X = -B +- √Δ / 2 * A\n\n";

    std::cout << "X' = -B + √Δ / 2 * A\n";
    std::cout << "X' = " << -b << " + √" << delta << " / 2 * " << a << '\n';
    std::cout << "X' = " << -b << " + " << sqrt(delta) << " / " << 2 * a << '\n';
    std::cout << "X' = " << -b + sqrt(delta) << " / " << 2 * a << '\n';
    std::cout << "X' = " << (-b + sqrt(delta)) / (2 * a) << "\n\n";
    x1 = (-b + sqrt(delta)) / (2 * a);

    std::cout << "X'' = -B - √Δ / 2 * A\n";
    std::cout << "X'' = " << -b << " - √" << delta << " / 2 * " << a << '\n';
    std::cout << "X'' = " << -b << " - " << sqrt(delta) << " / " << 2 * a << '\n';
    std::cout << "X'' = " << -b - sqrt(delta) << " / " << 2 * a << '\n';
    std::cout << "X'' = " << (-b - sqrt(delta)) / (2 * a) << "\n\n";
    x2 = (-b - sqrt(delta)) / (2 * a);

    std::cout << "Conjunto Solução: {" << x1 << ", " << x2 << "}\n";
    return 0;
}