#include <iostream>

unsigned long fatorial(int, int);

int main(void)
{
    // Combinação Cn,p = n!/p!(n-p)!
    unsigned long num, pos, C;

    std::cout << "Digite o número de elementos por conjunto: ";
    std::cin >> num;

    std::cout << "Digite o número de elementos por subconjunto: ";
    std::cin >> pos;

    if (pos > num)
        return 0;

    C = fatorial(num, pos) / fatorial(pos, pos);

    std::cout << "A combinação de " << num << " e " << pos << " é: " << C << '\n';
    return 0;
}

unsigned long fatorial(int num, int pos)
{
    // Fatorial feito para números maiores (86, 8 por exemplo.)
    unsigned long fator = 1;
    for (int i = num; i > num - pos; i--)
        fator *= i;
    return fator;
}