#include <iostream>

int main(void)
{
    const int QTD = 126;
    char letras[] = "AABBCCDD!!99      abcdefgabc";
    int alf[QTD]{0}, galf[QTD];

    for (int i = 0; i < QTD; i++)
        galf[i] = i;

    for (int i = 0; i < QTD; i++)
        for (int j = 0; j < sizeof(letras) - 1; j++)
            if (galf[i] == letras[j])
                alf[i]++;

    for (int i = 0; i < QTD; i++)
        if (alf[i] >= 2)
            std::cout << "Caractere " << (char)galf[i] << " repetido "
                      << alf[i] << " vezes." << std::endl;

    return 0;
}
