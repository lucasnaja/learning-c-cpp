#include <iostream>

int main(void)
{
    // Temperaturas: °C, K, °F
    short tipo;
    float *C = new float, *K = new float, *F = new float;
    std::cout << "Celsius: 1" << std::endl;
    std::cout << "Kelvin: 2" << std::endl;
    std::cout << "Fahrenheit: 3" << std::endl;
    std::cout << "Escolha o tipo de temperatura para converter: [1, 2, 3]: ";

    std::cin >> tipo;

    if (tipo == 1)
    {
        std::cout << "Digite a temperatura em °C: ";
        std::cin >> *C;
    }
    else if (tipo == 2)
    {
        std::cout << "Digite a temperatura em K: ";
        std::cin >> *K;
    }
    else if (tipo == 3)
    {
        std::cout << "Digite a temperatura em °F: ";
        std::cin >> *F;
    }
    else
        return 0;

    if (tipo == 1)
    {
        *K = *C + 273.15F;
        std::cout << *C << "°C convertido em K: " << *K << 'K' << std::endl;
        *F = (*C / 5.0F) * 9.0F + 32.0F;
        std::cout << *C << "°C convertido em °F: " << *F << "°F" << std::endl;
    }
    else if (tipo == 2)
    {
        *C = *K - 273.15F;
        std::cout << *K << "K convertido em °C: " << *C << "°C" << std::endl;
        *F = ((*K - 273.15F) / 5.0F) * 9.0F + 32.0F;
        std::cout << *K << "K convertido em °F: " << *F << "°F" << std::endl;
    }
    else if (tipo == 3)
    {
        *C = ((*F - 32.0F) / 9.0F) * 5.0F;
        std::cout << *F << "°F convertido em °C: " << *C << "°C" << std::endl;
        *K = ((*F - 32.0F) / 9.0F) * 5.0F + 273.15F;
        std::cout << *F << "°F convertido em K: " << *K << 'K' << std::endl;
    }
    else
        return 0;

    return 0;
}
