#include <iostream>
#include <string.h>

using namespace std;

int diaSemana(int, int, int);

int main(void)
{
    cout << "Digite uma data especial [dd/mm/aaaa]: ";
    int d, m, a;
    char ch;
    cin >> d >> ch >> m >> ch >> a;
    cout << "Data escolhida: " << d << '/' << m << '/' << a << endl;
    char diaString[][14] = {
        "Segunda-feira",
        "Terça-feira",
        "Quarta-feira",
        "Quinta-feira",
        "Sexta-feira",
        "Sábado",
        "Domingo"};
    cout << "Essa data específica caiu em: " << diaString[diaSemana(d, m, a)] << endl;
    return 0;
}

int diaSemana(int dia, int mes, int ano)
{
    int f = ano + dia + 3 * (mes - 1) - 1;
    if (mes < 3)
        ano--;
    else
        f -= int(0.4 * mes + 2.3);
    f += int(ano / 4) - int((ano / 100) + 1) * 0.75;
    f %= 7;
    return f;
}