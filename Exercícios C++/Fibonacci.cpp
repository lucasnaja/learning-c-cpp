#include <iostream>

int main(void)
{
    unsigned long int a, b, c;
    signed short int num;
    do
    {
        std::cout << "<----------------------------------------------------------------------------->\n";
        std::cout << "-> Digite a quantidade de números da Sequência de Fibonacci [0 = Cancelar]: ";
        std::cin >> num;

        if (num == 0)
            break;
        else if (num == 1)
            std::cout << "<- 0 ->\n";
        else if (num == 2)
            std::cout << "<- 0, 1 ->\n";
        else if (num > 2)
        {
            std::cout << "<- 0, 1, ";
            a = 0;
            b = 1;
            for (unsigned long int i = 0; i < num - 2; i++)
            {
                c = a + b;
                std::cout << c << (i == num - 3 ? "\0" : ", ");
                a = b;
                b = c;
            }
            std::cout << " ->\n";
        }
        else
            break;
    } while (1);

    std::cout << "<----------------------------------------------------------------------------->\n";

    return 0;
}