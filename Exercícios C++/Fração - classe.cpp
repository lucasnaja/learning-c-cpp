#include <iostream>

class fracao
{
    float n, d;

  public:
    fracao(float n = 1, float d = 1)
    {
        fracao::n = n;
        fracao::d = d == 0 ? 1 : d;
    }

    float numerador()
    {
        return fracao::n;
    }

    float denominador()
    {
        return fracao::d;
    }

    float operator+(fracao k)
    {
        return n / d + (k.n / k.d);
    }

    float operator-(fracao k)
    {
        return n / d - (k.n / k.d);
    }

    float operator*(fracao k)
    {
        return n / d * (k.n / k.d);
    }

    float operator/(fracao k)
    {
        return (n / d) / (k.n / k.d);
    }

    void operator()(int n = 0)
    {
        std::cout << (n == 0 ? fracao::n / d : d / fracao::n) << std::endl;
    }

    fracao operator++()
    {
        fracao::n++;
        return fracao(fracao::n, fracao::d);
    }

    fracao operator--()
    {
        fracao::n--;
        return fracao(fracao::n, fracao::d);
    }

    fracao operator++(int)
    {
        fracao::d++;
        return fracao(fracao::n, fracao::d);
    }

    fracao operator--(int)
    {
        fracao::d--;
        return fracao(fracao::n, fracao::d);
    }

    void print()
    {
        std::cout << n / d << std::endl;
    }

    void abs()
    {
        std::cout << n << '/' << d << std::endl;
    }
};

int main(void)
{
    fracao a = {50, 25};
    a.print();

    fracao b = a / fracao(5, 50);
    b.print();
    b.abs();
    return 0;
}
