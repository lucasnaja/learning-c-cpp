#include <iostream>
#define PI 3.14159

class Graus
{
    float graus;

  public:
    Graus(float g = 180.0f)
    {
        graus = g;
    }

    double retornaGraus()
    {
        return graus;
    }

    void print() const
    {
        std::cout << "Graus: " << graus << "°" << std::endl;
    }
};

class Radianos
{
    float rad;

  public:
    Radianos(float r = PI)
    {
        rad = r;
    }

    Radianos(Graus gr)
    {
        rad = gr.retornaGraus() * PI / 180.0;
    }

    operator Graus()
    {
        return 180.0 / PI * rad;
    }

    void print()
    {
        std::cout << "Radianos: " << rad << std::endl;
    }
};

int main(void)
{
    Graus g = 360;
    Radianos r;

    r = g;
    r.print();
    g = r;
    g.print();
    return 0;
}