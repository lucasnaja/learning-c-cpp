#include <iostream>
#include <string.h>

using namespace std;

void converteSTR(char[]);

int main()
{
    cout << "Digite seu nome: ";
    char nome[50];
    cin >> nome;
    cout << "Seu nome é: " << nome;
    converteSTR(nome);
    cout << ", e ao contrário é: " << nome << endl;
    return 0;
}

void converteSTR(char str[])
{
    int TAMANHO = strlen(str);
    char backup[TAMANHO];
    for (int i = TAMANHO; i > 0; i--)
        backup[i] = str[TAMANHO - i];
    for (int i = 0; i <= TAMANHO; i++)
        str[i] = backup[i];
}