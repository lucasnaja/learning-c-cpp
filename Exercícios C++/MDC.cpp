#include <iostream>
using namespace std;

const void mdc(int, int);
const void mdc(int, int, int);
const void mdc(int, int, int, int);

int main(void)
{
	while (1)
	{
		int n1 = 0, n2 = 0, n3 = 0, n4 = 0;
		cout << "<------------------------------------------->\n";
		cout << "Digite um número [0 = Cancelar]: ";
		cin >> n1;
		if (n1 == 0)
			break;
		cout << "Digite outro número [0 = Cancelar]: ";
		cin >> n2;
		if (n2 == 0)
			break;
		cout << "Digite outro número [0 = Parar]: ";
		cin >> n3;
		if (n3 == 0)
			mdc(n1, n2);
		else
		{
			cout << "Digite outro número [0 = Parar]: ";
			cin >> n4;
			if (n4 == 0)
				mdc(n1, n2, n3);
			else
				mdc(n1, n2, n3, n4);
		};
	}
	cout << "<------------------------------------------->\n";
	cout << "Finalizado com sucesso!\n";
	return 0;
}

const void mdc(int a, int b)
{
	int mdc = 1, cont = 1, ta = a, tb = b;
	while (a != 1 && b != 1)
	{
		int qtdDiv = 0;
		for (int i = cont; i > 0; i--)
			if (cont % i == 0)
				qtdDiv++;
		if (qtdDiv == 2)
			if (a % cont == 0 && b % cont == 0)
			{
				mdc *= cont;
				a /= cont;
				b /= cont;
			}
			else if (a % cont == 0)
				a /= cont;
			else if (b % cont == 0)
				b /= cont;
			else
				cont++;
		else
			cont++;
	}
	cout << "O MDC de " << ta << " e " << tb << " é: " << mdc << endl;
}

const void mdc(int a, int b, int c)
{
	int mdc = 1, cont = 1, ta = a, tb = b, tc = c;
	while (a != 1 && b != 1 && c != 1)
	{
		int qtdDiv = 0;
		for (int i = cont; i > 0; i--)
			if (cont % i == 0)
				qtdDiv++;
		if (qtdDiv == 2)
			if (a % cont == 0 && b % cont == 0 && c % cont == 0)
			{
				mdc *= cont;
				a /= cont;
				b /= cont;
				c /= cont;
			}
			else if (a % cont == 0)
				a /= cont;
			else if (b % cont == 0)
				b /= cont;
			else if (c % cont == 0)
				c /= cont;
			else
				cont++;
		else
			cont++;
	}
	cout << "O MDC de " << ta << " e " << tb << " e " << tc << " é: " << mdc << endl;
}

const void mdc(int a, int b, int c, int d)
{
	int mdc = 1, cont = 1, ta = a, tb = b, tc = c, td = d;
	while (a != 1 && b != 1 && c != 1 && d != 1)
	{
		int qtdDiv = 0;
		for (int i = cont; i > 0; i--)
			if (cont % i == 0)
				qtdDiv++;
		if (qtdDiv == 2)
			if (a % cont == 0 && b % cont == 0 && c % cont == 0 && d % cont == 0)
			{
				mdc *= cont;
				a /= cont;
				b /= cont;
				c /= cont;
				d /= cont;
			}
			else if (a % cont == 0)
				a /= cont;
			else if (b % cont == 0)
				b /= cont;
			else if (c % cont == 0)
				c /= cont;
			else if (d % cont == 0)
				d /= cont;
			else
				cont++;
		else
			cont++;
	}
	cout << "O MDC de " << ta << " e " << tb << " e " << tc << " e " << td << " é: " << mdc << endl;
}
