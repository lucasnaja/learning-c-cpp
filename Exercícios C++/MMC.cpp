#include <iostream>

int calculaMMC(int, int);
int calculaMMC(int, int, int);

int main(void)
{
    // Mínimo Múltiplo Comum
    unsigned long int num1, num2, num3;
    std::cout << "Digite o primeiro valor [0 = Cancelar]: ";
    std::cin >> num1;
    if (num1 <= 0)
        return 0;

    std::cout << "Digite o segundo valor [0 = Cancelar]: ";
    std::cin >> num2;
    if (num2 <= 0)
        return 0;

    std::cout << "Digite o terceiro valor [0 = Parar]: ";
    std::cin >> num3;

    if (num3 <= 0)
        std::cout << "O MMC de " << num1 << " e " << num2 << " é: " << calculaMMC(num1, num2) << std::endl;
    else
        std::cout << "O MMC de " << num1 << " e " << num2 << " e " << num3 << " é: " << calculaMMC(num1, num2, num3) << std::endl;

    return 0;
}

int calculaMMC(int num1, int num2)
{
    int mmc = 1, contador = 2;
    while (num1 != 1 || num2 != 1)
        if (num1 % contador != 0 && num2 % contador != 0)
            contador++;
        else
        {
            int qtdDiv = 0;
            for (int i = contador; i >= 1; i--)
                if (contador % i == 0)
                    qtdDiv++;

            if (qtdDiv != 2)
            {
                contador++;
                continue;
            }

            if (num1 % contador == 0)
                num1 /= contador;
            if (num2 % contador == 0)
                num2 /= contador;

            mmc *= contador;
        }
    return mmc;
}

int calculaMMC(int num1, int num2, int num3)
{
    int mmc = 1, contador = 2;
    while (num1 != 1 || num2 != 1 || num3 != 1)
        if (num1 % contador != 0 && num2 % contador != 0 && num3 % contador != 0)
            contador++;
        else
        {
            int qtdDiv = 0;
            for (int i = contador; i >= 1; i--)
                if (contador % i == 0)
                    qtdDiv++;

            if (qtdDiv != 2)
            {
                contador++;
                continue;
            }

            if (num1 % contador == 0)
                num1 /= contador;
            if (num2 % contador == 0)
                num2 /= contador;
            if (num3 % contador == 0)
                num3 /= contador;

            mmc *= contador;
        }
    return mmc;
}
