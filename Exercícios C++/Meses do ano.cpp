#include <iostream>

using namespace std;

class Mes
{
	static int mesAtual;
	char nome[12][11] = {
		"Janeiro", "Fevereiro",
		"Março", "Abril", "Maio",
		"Junho", "Julho", "Agosto",
		"Setembro", "Outubro",
		"Novembro", "Dezembro"};
	char abrNome[12][4] = {
		"Jan", "Fev", "Mar", "Abr",
		"Mai", "Jun", "Jul", "Ago",
		"Set", "Out", "Nov", "Dez"};
	int nDias[12] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

  public:
	Mes(int mA = 1)
	{
		Mes::mesAtual = mA;
	}
	void infoMes(int mA = Mes::mesAtual) const
	{
		cout << nome[mA - 1] << endl
			 << "Dias do mês: " << nDias[mA - 1] << endl;
	}
	void infoMes(string nM) const
	{
		for (int i = 0; i < 12; i++)
			if (nM == abrNome[i])
				cout << nome[i] << endl
					 << "Dias do mês: " << nDias[i] << endl;
	}
};

int Mes::mesAtual;

int main(void)
{
	const Mes m1(7);
	m1.infoMes();
	return 0;
}
