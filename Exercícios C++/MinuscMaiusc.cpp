#include <iostream>
#include <string.h>

using namespace std;

void converteMaiusc(char[]);

int main()
{
    // 'a' = 97
    // 'A' = 65
    cout << "Digite um texto em minúsculo: ";
    char texto[50];
    cin >> texto;
    converteMaiusc(texto);
    cout << texto << endl;
    return 0;
}

void converteMaiusc(char str[])
{
    int tamanho = strlen(str);
    for (int i = 0; i < tamanho; i++)
    {
        str[i] = (int(str[i]) - 32 < 65 ? str[i] + 32 : str[i] - 32);
    }
}