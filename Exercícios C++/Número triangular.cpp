#include <iostream>

int main(void)
{
	int num;
	std::cout << "Digite um número: ";
	std::cin >> num;

	for (int i = 0, j = 1; i <= num; i += j, j++)
	{
		if (i == num)
			std::cout << "O número " << num << " é um número triangular!" << std::endl;
	}
	return 0;
}