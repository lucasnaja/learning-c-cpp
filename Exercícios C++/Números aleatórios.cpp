#include <iostream>
#include <stdlib.h>
#include <time.h>

int i = 1, f = 9;

inline int Random(int i = ::i, int f = ::f)
{
    srand(time(NULL));
    return int(rand() % (f + 1) + i);
}

inline void SetRandom(int i, int f)
{
    ::i = i;
    ::f = f - i;
}

inline void InfoRandom()
{
    std::cout << "Será gerado um número aleatório de " << ::i << " até " << ::i + ::f << "!\n";
}

int main()
{
    SetRandom(1, 100);
    InfoRandom();
    std::cout << "Número gerado: " << Random() << std::endl;
    return 0;
}