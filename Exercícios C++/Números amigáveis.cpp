#include <iostream>

bool isAmicable(int, int);

int main(void)
{
    int n1, n2;
    std::cout << "Digite o primeiro número: ";
    std::cin >> n1;
    std::cout << "Digite o segundo número: ";
    std::cin >> n2;
    std::cout << "Esses dois números são amigáveis? " << (isAmicable(n1, n2) ? "SIM!" : "NÃO!") << std::endl;
    return 0;
}

bool isAmicable(int n1, int n2)
{
    int s1 = 0, s2 = 0;
    for (int i = n1; i > 0; i--)
        if (n1 % i == 0)
            s1 += i;
    for (int i = n2; i > 0; i--)
        if (n2 % i == 0)
            s2 += i;
    return (s1 == s2 ? 1 : 0);
}