#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

inline int randint(int i, int f)
{
	srand(time(NULL));
	return rand() % (f - i) + i;
}

int main(void)
{
	system("clear");
	std::cout << "<- UNITEL PROGRAM ->\n";
	unsigned long int num = 0;
	while (1)
	{
		num++;
		char ch;
		ch = randint(97, 123);
		std::cout << "Número gerado: " << num << ch << std::endl;
		usleep(1000000);
	}
	return 0;
}
