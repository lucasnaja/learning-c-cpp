#include <iostream>
#include <string.h>

using namespace std;

void replace(char[], char, char);

int main()
{
    cout << "Digite o texto: ";
    char txt[50];
    cin >> txt;
    replace(txt, 'u', 'U');
    replace(txt, 'l', 'L');
    replace(txt, 'c', 'C');
    replace(txt, 'a', 'A');
    replace(txt, 's', 'S');
    cout << txt << endl;
    return 0;
}

void replace(char str[], char v, char n)
{
    int TAM = strlen(str);
    for (int i = 0; i < TAM; i++)
        if (str[i] == v)
            str[i] = n;
}