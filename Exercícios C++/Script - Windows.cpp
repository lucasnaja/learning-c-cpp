#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <locale.h>

/* Mini Script criado em C++
   16/06/2018 
*/

const void mdc(int, int);
const void mdc(int, int, int);
const void mdc(int, int, int, int);

int main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    do
    {
        std::cout << "<--------------------------------->\n";
        std::cout << "         <- Script V0.2->\n";
        std::cout << "<--------------------------------->\n";
        std::cout << "[0] -> Cancelar\n";
        std::cout << "[1] -> Limpar terminal\n";
        std::cout << "[2] -> Formula de Bhaskara\n";
        std::cout << "[3] -> Sequencia de Fibonacci\n";
        std::cout << "[4] -> Numero elevado ao expoente\n";
        std::cout << "[5] -> Maximo Divisor Comum\n";

        std::cout << "<--------------------------------->\n";
        std::cout << "-> Digite um numero [0-5]: ";
        signed short int num;
        std::cin >> num;

        if (num == 0)
        {
            std::cout << "-> Finalizado com sucesso!\n"; // Finalizar operacao
            break;
        }
        else if (num == 1)
            system("cls"); // Limpar tela
        else if (num == 2)
        {
            // Formula de Bhaskara
            std::cout << "\n<------------------------------------------>\n";
            do
            {
                signed long int a, b, c;
                long double delta, x1, x2;

                std::cout << "-> Digite o valor de A [0 = Cancelar]: ";
                std::cin >> a;

                if (a == 0)
                {
                    std::cout << "<------------------------------------------>\n<- CANCELADO ->\n";
                    break;
                }

                std::cout << "-> Digite o valor de B: ";
                std::cin >> b;
                std::cout << "-> Digite o valor de C: ";
                std::cin >> c;

                std::cout << "\nDelta = B * B - 4 * A * C\n";
                std::cout << "Delta = " << b << " * " << b << " - 4 * " << a << " * " << c << '\n';

                char sinal;
                if (4 * a * c < 0)
                    sinal = '+';
                else
                    sinal = '-';

                std::cout << "Delta = " << b * b << ' ' << sinal << ' ' << (4 * a * c < 0 ? -4 * a * c : 4 * a * c) << '\n';
                std::cout << "Delta = " << b * b - 4 * a * c << '\n';

                delta = b * b - 4 * a * c;
                if (delta < 0)
                {
                    std::cout << "\n-> Para Delta menor que 0, nao existem raizes reais.\n";
                    std::cout << "<------------------------------------------>\n";
                    continue;
                }
                else if (delta == 0)
                    std::cout << "\n-> Para Delta igual a 0, existem duas raizes reais iguais.\n\n";
                else
                    std::cout << "\n-> Para Delta maior que 0, existem duas raizes reais distintas.\n\n";

                std::cout << "X = -B +- sqrt(Delta) / 2 * A\n\n";

                std::cout << "X' = -B + sqrt(Delta) / 2 * A\n";
                std::cout << "X' = " << -b << " + sqrt(" << delta << ") / 2 * " << a << '\n';
                std::cout << "X' = " << -b << " + " << sqrt(delta) << " / " << 2 * a << '\n';
                std::cout << "X' = " << -b + sqrt(delta) << " / " << 2 * a << '\n';
                std::cout << "X' = " << (-b + sqrt(delta)) / (2 * a) << "\n\n";
                x1 = (-b + sqrt(delta)) / (2 * a);

                std::cout << "X'' = -B - sqrt(Delta) / 2 * A\n";
                std::cout << "X'' = " << -b << " - sqrt(" << delta << ") / 2 * " << a << '\n';
                std::cout << "X'' = " << -b << " - " << sqrt(delta) << " / " << 2 * a << '\n';
                std::cout << "X'' = " << -b - sqrt(delta) << " / " << 2 * a << '\n';
                std::cout << "X'' = " << (-b - sqrt(delta)) / (2 * a) << "\n\n";
                x2 = (-b - sqrt(delta)) / (2 * a);

                std::cout << "Conjunto Solucao: {" << x1 << ", " << x2 << "}\n";
                std::cout << "<------------------------------------------>\n";
            } while (1);
        }
        else if (num == 3)
        {
            // Sequencia de Fibonacci
            unsigned long int a, b, c;
            signed int num;
            std::cout << "\n<----------------------------------------------------------------------------->\n";
            do
            {
                std::cout << "-> Digite a quantidade de numeros da Sequencia de Fibonacci [0 = Cancelar]: ";
                std::cin >> num;

                if (num == 1)
                    std::cout << "<- 0 ->\n";
                else if (num == 2)
                    std::cout << "<- 0, 1 ->\n";
                else if (num > 2)
                {
                    std::cout << "<- 0, 1, ";
                    a = 0;
                    b = 1;
                    for (unsigned long int i = 0; i < num - 2; i++)
                    {
                        c = a + b;
                        std::cout << c << (i == num - 3 ? "\0" : ", ");
                        a = b;
                        b = c;
                    }
                    std::cout << " ->\n";
                }

                std::cout << "<----------------------------------------------------------------------------->\n";
                if (num <= 0)
                    break;
            } while (1);
            std::cout << "<- CANCELADO ->\n";
        }
        else if (num == 4)
        {
            // Numero elevado ao expoente
            std::cout << "\n<------------------------------------------->\n";
            do
            {
                unsigned long int numBase, numExp, resultado;

                std::cout << "-> Digite o numero-base [0 = Cancelar]: ";
                std::cin >> numBase;

                if (numBase <= 0)
                {
                    std::cout << "<------------------------------------------->\n";
                    std::cout << "<- CANCELADO ->\n";
                    break;
                }

                std::cout << "-> Digite o Expoente do numero-base: ";
                std::cin >> numExp;

                resultado = int(pow(numBase, numExp));

                std::cout << "<- " << numBase << " elevado a " << numExp << " e " << resultado << " -> \n";
                std::cout << "<------------------------------------------->\n";
            } while (1);
        }
        else if (num == 5)
        {
            // Maximo Divisor Comum
            while (1)
            {
                int n1 = 0, n2 = 0, n3 = 0, n4 = 0;
                std::cout << "<-------------------------------------->\n";
                std::cout << "Digite um numero [0 = Cancelar]: ";
                std::cin >> n1;
                if (n1 == 0)
                    break;
                std::cout << "Digite outro numero [0 = Cancelar]: ";
                std::cin >> n2;
                if (n2 == 0)
                    break;
                std::cout << "Digite outro numero [0 = Parar]: ";
                std::cin >> n3;
                if (n3 == 0)
                    mdc(n1, n2);
                else
                {
                    std::cout << "Digite outro numero [0 = Parar]: ";
                    std::cin >> n4;
                    if (n4 == 0)
                        mdc(n1, n2, n3);
                    else
                        mdc(n1, n2, n3, n4);
                }
            }
            std::cout << "<-------------------------------------->\n";
            std::cout << "<- CANCELADO ->\n";
        }
        else
        {
            std::cout << "-> Nenhuma funcao correspondente!\n";
            break;
        }
    } while (1);

    return 0;
}

const void mdc(int a, int b)
{
    int mdc = 1, cont = 1, ta = a, tb = b;
    while (a != 1 && b != 1)
    {
        int qtdDiv = 0;
        for (int i = cont; i > 0; i--)
            if (cont % i == 0)
                qtdDiv++;
        if (qtdDiv == 2)
            if (a % cont == 0 && b % cont == 0)
            {
                mdc *= cont;
                a /= cont;
                b /= cont;
            }
            else if (a % cont == 0)
                a /= cont;
            else if (b % cont == 0)
                b /= cont;
            else
                cont++;
        else
            cont++;
    }
    std::cout << "O MDC de " << ta << " e " << tb << " e: " << mdc << std::endl;
}

const void mdc(int a, int b, int c)
{
    int mdc = 1, cont = 1, ta = a, tb = b, tc = c;
    while (a != 1 && b != 1 && c != 1)
    {
        int qtdDiv = 0;
        for (int i = cont; i > 0; i--)
            if (cont % i == 0)
                qtdDiv++;
        if (qtdDiv == 2)
            if (a % cont == 0 && b % cont == 0 && c % cont == 0)
            {
                mdc *= cont;
                a /= cont;
                b /= cont;
                c /= cont;
            }
            else if (a % cont == 0)
                a /= cont;
            else if (b % cont == 0)
                b /= cont;
            else if (c % cont == 0)
                c /= cont;
            else
                cont++;
        else
            cont++;
    }
    std::cout << "O MDC de " << ta << " e " << tb << " e " << tc << " e: " << mdc << std::endl;
}

const void mdc(int a, int b, int c, int d)
{
    int mdc = 1, cont = 1, ta = a, tb = b, tc = c, td = d;
    while (a != 1 && b != 1 && c != 1 && d != 1)
    {
        int qtdDiv = 0;
        for (int i = cont; i > 0; i--)
            if (cont % i == 0)
                qtdDiv++;
        if (qtdDiv == 2)
            if (a % cont == 0 && b % cont == 0 && c % cont == 0 && d % cont == 0)
            {
                mdc *= cont;
                a /= cont;
                b /= cont;
                c /= cont;
                d /= cont;
            }
            else if (a % cont == 0)
                a /= cont;
            else if (b % cont == 0)
                b /= cont;
            else if (c % cont == 0)
                c /= cont;
            else if (d % cont == 0)
                d /= cont;
            else
                cont++;
        else
            cont++;
    }
    std::cout << "O MDC de " << ta << " e " << tb << " e " << tc << " e " << td << " e: " << mdc << std::endl;
}
