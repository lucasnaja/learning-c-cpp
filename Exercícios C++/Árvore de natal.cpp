#include <iostream>

int main(void)
{
    /*      *
           * *
          * * *
         * * * *
        * * * * *
       * * * * * *
      * * * * * * *       */

    int tam = 10;
    for (int i = 0; i < tam; i++)
    {
        for (int j = 0; j < tam * 2; j++)
            if (j == tam - i)
                for (int k = i; k >= 0; k--)
                    std::cout << "* ";
            else
                std::cout << ' ';
        std::cout << '\n';
    }
    return 0;
}
