#include <iostream>
#include <iomanip>

int main(void)
{
    float peso, altura, imc;
    std::cout << "Digite seu PESO (KG, Ex: 78.5): ";
    std::cin >> peso;

    std::cout << "Digite sua ALTURA (M, Ex: 1.72): ";
    std::cin >> altura;

    imc = peso / (altura * altura);
    std::cout << "Seu IMC é de: " << std::setiosflags(std::ios::fixed) << std::setiosflags(std::ios::showpoint) << std::setprecision(2) << imc << std::endl;

    std::cout << "Seu nivel de IMC é: ";

    if (imc < 17)
        std::cout << "Muito abaixo do peso." << std::endl;
    else if (imc < 18.5)
        std::cout << "Abaixo do peso." << std::endl;
    else if (imc < 25)
        std::cout << "Peso normal." << std::endl;
    else if (imc < 30)
        std::cout << "Acima do peso." << std::endl;
    else if (imc < 35)
        std::cout << "Obesidade I" << std::endl;
    else if (imc < 40)
        std::cout << "Obesidade II (Severa)." << std::endl;
    else
        std::cout << "Obesidade III (Mórbida)." << std::endl;

    return 0;
}
