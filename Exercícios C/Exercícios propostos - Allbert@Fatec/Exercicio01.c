#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    float i = 10;

    do
    {
        printf("%f°C = %.2fF\n", i, 32 + 1.8 * i);
        i += 10;
    } while (i <= 100);
}