#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    float chicoTamanho = 1.5, zeTamanho = 1.1;
    int anos = 0;

    while (zeTamanho < chicoTamanho) {
        chicoTamanho += .2;
        zeTamanho += .3;
        
        anos++;
    }

    printf("Levará %i anos para Zé ser maior que Chico.\n", anos);
}
