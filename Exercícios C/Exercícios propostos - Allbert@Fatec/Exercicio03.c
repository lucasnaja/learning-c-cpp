#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int i, qtdImpares = 0, pares = 0, qtdPares = 0;

    do
    {
        printf("Digite o número: ");
        scanf("%i", &i);

        if (i % 2 == 0)
        {
            pares += i;
            qtdPares++;
        }
        else
        {
            qtdImpares++;
        }
    } while (i != 0);

    printf("Quantidade de números ímpares: %i\n", qtdImpares);
    printf("Quantidade de números pares: %i\n", --qtdPares);
    printf("Média dos números pares: %.2f\n", (float)pares / qtdPares);
}