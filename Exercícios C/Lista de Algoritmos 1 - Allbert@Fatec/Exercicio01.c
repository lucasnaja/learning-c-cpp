#include <stdio.h>
#include <locale.h>

void main()
{
  setlocale(LC_ALL, "Portuguese_Brazil");
  float area, base, altura;

  printf("Digite a base do triângulo: ");
  scanf("%f", &base);

  printf("Digite a altura do triângulo: ");
  scanf("%f", &altura);

  area = base * altura / 2;

  printf("A área do triângulo é: %.2fm\n", area);
}
