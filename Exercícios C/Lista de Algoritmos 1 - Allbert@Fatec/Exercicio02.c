#include <stdio.h>
#include <locale.h>

void main()
{
  setlocale(LC_ALL, "Portuguese_Brazil");
  float valor;

  printf("Digite o valor da compra: ");
  scanf("%f", &valor);

  printf("O valor de cada prestação é: %.2fR$\n", valor / 5);
}
