#include <stdio.h>
#include <locale.h>

void main()
{
  setlocale(LC_ALL, "Portuguese_Brazil");
  float saldo, reajuste = 0.075;

  printf("Digite seu saldo de aplicação: ");
  scanf("%f", &saldo);

  printf("O saldo é de %.2fR$ e com o reajuste de %.2f%% fica %.2fR$\n", saldo, reajuste * 100, saldo + saldo * reajuste);
}
