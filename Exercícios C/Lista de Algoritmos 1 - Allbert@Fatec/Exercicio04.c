#include <stdio.h>
#include <locale.h>

int main()
{
  setlocale(LC_ALL, "Portuguese_Brazil");
  float dolares, cotacao;

  printf("Digite os dólares guardados: ");
  scanf("%f", &dolares);

  printf("Digite a cotação atual do dólar: ");
  scanf("%f", &cotacao);

  printf("A conversão de %.2f$ para reais é: %.2fR$\n", dolares, dolares * cotacao);
  return 0;
}
