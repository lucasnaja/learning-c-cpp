#include <stdio.h>
#include <locale.h>

void main()
{
  setlocale(LC_ALL, "Portuguese_Brazil");
  float valor, reajuste = 0.09;

  printf("Digite o valor do produto: ");
  scanf("%f", &valor);

  printf("O valor é de %.2fR$ e com o reajuste de %.2f%% fica %.2fR$\n", valor, reajuste * 100, valor - valor * reajuste);
}
