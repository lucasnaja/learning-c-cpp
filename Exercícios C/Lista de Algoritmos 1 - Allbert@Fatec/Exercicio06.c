#include <stdio.h>
#include <locale.h>

void main()
{
  setlocale(LC_ALL, "Portuguese_Brazil");
  int anoNasc, anoAtual = 2019;

  printf("Digite o ano de nascimento da pessoa [yyyy]: ");
  scanf("%i", &anoNasc);

  printf("A pessoa tem %i anos e terá %i anos em 2028\n", anoAtual - anoNasc, 2028 - anoNasc);
}
