#include <stdio.h>
#include <math.h>
#include <locale.h>

void main()
{
  setlocale(LC_ALL, "Portuguese_Brazil");
  float lado;

  printf("Digite o lado do quadrado: ");
  scanf("%f", &lado);

  printf("Perímetro: %.1fm\nÁrea: %.1fm\nDiagonal: %.1fm\n", lado * 4, lado * lado, lado * pow(2, 1/2.0));
}
