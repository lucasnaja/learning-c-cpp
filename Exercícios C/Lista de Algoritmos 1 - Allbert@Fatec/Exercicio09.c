#include <stdio.h>
#include <locale.h>

void main()
{
  setlocale(LC_ALL, "Portuguese_Brazil");
  float peso, altura;

  printf("Digite o peso da pessoa [Em KG]: ");
  scanf("%f", &peso);

  printf("Digite a altura da pessoa [Em Metros]: ");
  scanf("%f", &altura);

  printf("A índice de massa corporal é: %.2f\n", peso / (altura * altura));
}
