#include <stdio.h>
#include <locale.h>

void main()
{
  setlocale(LC_ALL, "Portuguese_Brazil");
  float sal_fixo, totalVendas;
  int num;

  printf("Digite o número do vendedor: ");
  scanf("%i", &num);

  printf("Digite o salário fixo do vendedor: ");
  scanf("%f", &sal_fixo);

  printf("Digite o total de vendas: ");
  scanf("%f", &totalVendas);

  printf("O número do vendedor é %i, o salário é %.2fR$ e o valor final é %.2fR$\n", num, sal_fixo, sal_fixo + 0.05 * totalVendas);
}
