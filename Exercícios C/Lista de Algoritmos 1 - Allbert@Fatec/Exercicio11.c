#include <stdio.h>
#include <locale.h>

void main()
{
  setlocale(LC_ALL, "Portuguese_Brazil");
  float km, v;

  printf("Digite a distância entre os dois pontos: ");
  scanf("%f", &km);

  printf("Digite a velocidade: ");
  scanf("%f", &v);

  printf("Você chegará lá em %fh\n", km / v);
}
