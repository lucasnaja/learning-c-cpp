#include <stdio.h>
#include <locale.h>

void main()
{
  setlocale(LC_ALL, "Portuguese_Brazil");
  float F;

  printf("Digite a temperatura em graus fahrenheit: ");
  scanf("%f", &F);

  printf("A temperatura %f°F em °C é: %f°C\n", F, (F - 32) * 5/9);
}
