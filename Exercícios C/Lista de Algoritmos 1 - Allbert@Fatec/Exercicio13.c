#include <stdio.h>
#include <locale.h>

void main()
{
  setlocale(LC_ALL, "Portuguese_Brazil");
  long long unsigned int d, h, m, s;

  printf("Digite os segundos: ");
  scanf("%i", &s);
  
  d = s / 86400;
  s -= d * 86400;
  h = s / 3600;
  s -= h * 3600;
  m = s / 60;
  s -= m * 60;

  printf("%i dias, %i horas, %i minutos e %i segundos\n", d, h, m, s);
}
