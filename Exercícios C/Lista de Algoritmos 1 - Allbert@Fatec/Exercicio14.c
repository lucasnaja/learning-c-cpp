#include <stdio.h>
#include <locale.h>

void main()
{
  setlocale(LC_ALL, "Portuguese_Brazil");
  float comp, altura, qtdH, qtdV;

  printf("Digite a altura da parede [Em M]: ");
  scanf("%f", &altura);

  printf("Digite o comprimento da parede [Em M]: ");
  scanf("%f", &comp);

  qtdH = comp * 100 / 32.5;
  qtdV = altura * 100 / 22.5;

  printf("Você precisará utilizar %.0f tijolos na coluna, %.0f na linha e %.0f tijolos nessa construção\n", qtdV, qtdH,  qtdH * qtdV);
}
