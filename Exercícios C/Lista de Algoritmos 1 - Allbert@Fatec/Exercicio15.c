#include <stdio.h>
#include <locale.h>

void main()
{
  setlocale(LC_ALL, "Portuguese_Brazil");
  int anos, numCigarros;
  float preco, precoTotal;

  printf("Digite a quantiade de anos que fuma: ");
  scanf("%i", &anos);

  printf("Digite a quantiade de cigarros que fuma por dia: ");
  scanf("%i", &numCigarros);

  printf("Digite o preço da carteira de cigarro: ");
  scanf("%f", &preco);
  
  precoTotal = (float)(365 * anos * numCigarros / 20) * preco;

  printf("Em %i anos você gastou %.2fR$ em cigarros\n", anos, precoTotal);
}
