#include <stdio.h>
#include <locale.h>

void main()
{
  setlocale(LC_ALL, "Portuguese_Brazil");
  float salMinimo, precoTotal;
  int qtdWatts;

  printf("Digite o valor do salário mínimo: ");
  scanf("%f", &salMinimo);

  printf("Digite a quantidade de QuiloWatts gasta: ");
  scanf("%i", &qtdWatts);

  precoTotal = qtdWatts * (salMinimo * 0.005);

  printf("O valor do kilowatt de energia gasto é: %.2fR$, o preço total é: %.2fR$ e com desconto de 15%% é: %.2fR$\n", salMinimo * 0.005, precoTotal, precoTotal - precoTotal * 0.15);
}
