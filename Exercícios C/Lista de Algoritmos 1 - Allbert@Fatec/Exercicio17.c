#include <stdio.h>
#include <locale.h>

void main()
{
  setlocale(LC_ALL, "Portuguese_Brazil");
  int qtdApartamentos;
  float valorDiaria;

  printf("Digite a quantidade de apartamentos: ");
  scanf("%i", &qtdApartamentos);

  printf("Digite o valor da diária: ");
  scanf("%f", &valorDiaria);

  printf("O valor da diária com desconto é: %.2fR$\n", valorDiaria - valorDiaria * 0.25);

  printf("O valor a ser arrecadado caso ocupe 100%% dos apartamentos: %.2fR$\n", qtdApartamentos * (valorDiaria - valorDiaria * 0.25));
  printf("O valor a ser arrecadado caso ocupe 70%% dos apartamentos: %.2fR$\n", qtdApartamentos * 0.7 * (valorDiaria - valorDiaria * 0.25));
  printf("Valor que o hotel deixará de arrecadar em virtude da promoção: %.2fR$\n", qtdApartamentos * (valorDiaria)-qtdApartamentos * (valorDiaria - valorDiaria * 0.25));
}
