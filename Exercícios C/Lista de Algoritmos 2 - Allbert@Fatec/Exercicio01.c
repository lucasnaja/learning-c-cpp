#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int A, B;

    printf("Digite o valor A: ");
    scanf("%i", &A);

    printf("Digite o valor B: ");
    scanf("%i", &B);

    if (A == B)
    {
        printf("Os valores são iguais e a soma de %i + %i = %i\n", A, B, A + B);
    }
    else
    {
        printf("Os valores são diferentes e produto de %i * %i = %i\n", A, B, A * B);
    }
}