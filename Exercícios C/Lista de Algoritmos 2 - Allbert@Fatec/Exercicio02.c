#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int num;

    printf("Digite um número: ");
    scanf("%i", &num);

    if (num % 10 == 0 || num % 5 == 0 || num % 2 == 0)
    {
        if (num % 10 == 0)
        {
            printf("%i é divisível por 10.\n", num);
        }

        if (num % 5 == 0)
        {
            printf("%i é divisível por 5.\n", num);
        }

        if (num % 2 == 0)
        {
            printf("%i é divisível por 2.\n", num);
        }
    }
    else
    {
        printf("%i não é divisível por 10, 5 e 2.\n", num);
    }
}