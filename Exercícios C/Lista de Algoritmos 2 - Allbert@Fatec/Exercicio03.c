#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    float sal_bruto, prestacao;

    printf("Digite seu salário bruto: ");
    scanf("%f", &sal_bruto);

    printf("Digite seu empréstimo: ");
    scanf("%f", &prestacao);

    if (prestacao / sal_bruto > .3f)
    {
        printf("Empréstimo não pode ultrapassar 30%% do salário.\n");
    }
    else
    {
        printf("Você pode fazer esse empréstimo.\n");
    }
}