#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    float altura;
    char sexo;

    printf("Digite seu sexo [M ou F]: ");
    scanf("%c", &sexo);

    printf("Digite sua altura: ");
    scanf("%f", &altura);

    switch (sexo)
    {
    case 'm':
    case 'M':
        printf("Seu peso ideal é: %.2f\n", 72.7f * altura - 58);
        break;
    case 'f':
    case 'F':
        printf("Seu peso ideal é: %.2f\n", 62.1f * altura - 44.7f);
        break;
    default:
        printf("Sexo inválido!\n");
    }
}