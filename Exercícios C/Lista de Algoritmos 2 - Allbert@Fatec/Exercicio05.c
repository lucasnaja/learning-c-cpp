#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    float PE;
    int CP;

    printf("Digite o Preço de Etiqueta: ");
    scanf("%f", &PE);

    printf("Digite o Código de Pagamento: ");
    scanf("%i", &CP);

    switch (CP)
    {
    case 1:
        printf("À vista em dinheiro ou cheque, com 10%% de desconto: de %.2fR$ para %.2fR$\n", PE, .9 * PE);
        break;
    case 2:
        printf("À vista com cartão de crédito, com 5% de desconto: de %.2f para %.2f\n", PE, .95 * PE);
        break;
    case 3:
        printf("Em 2 vezes, preço normal de etiqueta sem juros: %.2f\n", PE / 2.0);
        break;
    case 4:
        printf("Em 3 vezes, preço de etiqueta com acréscimo de 10%%: %.2f\n", PE * 1.1 / 3.0);
        break;
    default:
        printf("Código de Pagamento inválido.\n");
    }
}