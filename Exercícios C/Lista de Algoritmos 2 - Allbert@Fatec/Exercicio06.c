#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int peso;

    printf("Digite o peso dos peixes pescados: ");
    scanf("%i", &peso);

    if (peso > 50)
    {
        int pesoExcedido = peso - 50;
        printf("O peso regulamentado foi excedido em %i. Multa: R$%i,00\n", pesoExcedido, pesoExcedido * 4);
    }
    else
    {
        printf("Peso dentro do regulamentado.\n");
    }
}