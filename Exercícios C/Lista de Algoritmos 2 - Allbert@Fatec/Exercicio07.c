#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    char turno;

    printf("Que turno você estuda?\nM - Matutino\nV - Verpertino\nN - Noturno\nTurno: ");
    scanf("%c", &turno);

    switch (turno)
    {
    case 'm':
    case 'M':
        printf("Bom dia!\n");
        break;
    case 'v':
    case 'V':
        printf("Boa tarde!\n");
        break;
    case 'n':
    case 'N':
        printf("Bom noite!\n");
        break;
    default:
        printf("Valor inválido!");
    }
}