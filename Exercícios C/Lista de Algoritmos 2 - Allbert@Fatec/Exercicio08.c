#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    float peso;
    int codigo;

    printf("Digite seu peso na terra: ");
    scanf("%f", &peso);

    printf("Código dos planetas:\n1 - Mercúrio\n2 - Vênus\n3 - Marte\n4 - Júpiter\n5 - Saturno\nDigite o código do planeta: ");
    scanf("%i", &codigo);

    if (codigo == 1)
    {
        printf("Seu peso em Mercúrio é %.2fkg.\n", peso * .37);
    }
    else if (codigo == 2)
    {
        printf("Seu peso em Vênus é %.2fkg.\n", peso * .88);
    }
    else if (codigo == 3)
    {
        printf("Seu peso em Marte é %.2fkg.\n", peso * .38);
    }
    else if (codigo == 4)
    {
        printf("Seu peso em Júpiter é %.2fkg.\n", peso * 2.64);
    }
    else if (codigo == 5)
    {
        printf("Seu peso em Saturno é %.2fkg.\n", peso * 1.15);
    }
    else
    {
        printf("Código de planeta inválido.\n");
    }
}