#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    float velocidade, velocidadePermitida, velocidadeAcima;

    printf("Digite a velocidade máxima permitida: ");
    scanf("%f", &velocidadePermitida);

    printf("Digite a velocidade do carro: ");
    scanf("%f", &velocidade);

    velocidadeAcima = velocidade - velocidadePermitida;

    if (velocidadeAcima > 0)
    {
        if (velocidadeAcima <= 10)
        {
            printf("Você ultrapassou em %.2fkm e a multa é de R$50,00.\n", velocidadeAcima);
        }
        else if (velocidadeAcima <= 30)
        {
            printf("Você ultrapassou em %.2fkm e a multa é de R$100,00.\n", velocidadeAcima);
        }
        else
        {
            printf("Você ultrapassou em %.2fkm e a multa é de R$200,00.\n", velocidadeAcima);
        }
    }
    else if (velocidadeAcima == 0)
    {
        printf("Você não ultrapassou. Você chegou no limite, mas não ultrapassou.\n");
    }
    else
    {
        printf("Você não ultrapassou. Faltou %.2fkm para você ultrapassar.\n", -velocidadeAcima + 0.01f);
    }
}