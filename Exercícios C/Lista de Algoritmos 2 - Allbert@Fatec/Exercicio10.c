#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    float peso;

    printf("Digite o peso da peça: ");
    scanf("%f", &peso);

    if (peso > 50)
    {
        printf("A peça pesa mais de 50kg e será aproveitado 85%%. 85%% de %.1fkg = %.1fkg\n", peso, .85 * peso);
    }
    else if (peso > 20 && peso <= 50)
    {
        printf("A peça pesa mais que 20kg e menos de 51kg e será aproveitado 60%%. 60%% de %.1fkg = %.2fkg\n", peso, .6 * peso);
    }
    else if (peso > 10 && peso <= 20)
    {
        printf("A peça pesa mais que 10kg e menos de 21kg e será aproveitado 30%%. 30%% de %.1fkg = %.2fkg\n", peso, .3 * peso);
    }
    else
    {
        printf("A peça pesa menos que 11kg e não será aproveitada.\n");
    }
}