#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    float peso, altura, imc;

    printf("Digite o seu peso: ");
    scanf("%f", &peso);

    printf("Digite sua altura: ");
    scanf("%f", &altura);

    imc = peso / (altura * altura);

    if (imc < 20)
    {
        printf("Abaixo do peso ideal. IMC: %.2f\n", imc);
    }
    else if (imc < 25)
    {
        printf("Peso normal. IMC: %.2f\n", imc);
    }
    else if (imc < 30)
    {
        printf("Excesso de peso. IMC: %.2f\n", imc);
    }
    else if (imc < 35)
    {
        printf("Obesidade. IMC: %.2f\n", imc);
    }
    else
    {
        printf("Obesidade Mórbida. IMC: %.2f\n", imc);
    }
}