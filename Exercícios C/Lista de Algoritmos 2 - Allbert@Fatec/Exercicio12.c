#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    char area;
    float salario, indice;

    printf("P - Produção\nA - Administração\nDigite a área do funcionário: ");
    scanf("%c", &area);

    printf("Digite o salário do funcionário: ");
    scanf("%f", &salario);

    switch (area)
    {
    case 'p':
    case 'P':
        if (salario <= 700)
        {
            indice = .15;
        }
        else if (salario <= 1800)
        {
            indice = .1;
        }
        else if (salario <= 2500)
        {
            indice = .07;
        }
        else
        {
            indice = 0;
        }

        printf("O funcionário de Produção tem um salário de R$%.2f e com um aumento de %.2f%%, irá aumentar R$%.2f e ficar R$%.2f\n", salario, indice * 100, salario * indice, salario + salario * indice);
        break;
    case 'a':
    case 'A':
        if (salario <= 700)
        {
            indice = .18;
        }
        else if (salario <= 1800)
        {
            indice = .12;
        }
        else if (salario <= 2500)
        {
            indice = .08;
        }
        else
        {
            indice = 0.05;
        }

        printf("O funcionário de Administração tem um salário de R$%.2f e com um aumento de %.2f%%, irá aumentar R$%.2f e ficar R$%.2f\n", salario, indice * 100, salario * indice, salario + salario * indice);
        break;
    default:
        printf("Área inválida.\n");
    }
}