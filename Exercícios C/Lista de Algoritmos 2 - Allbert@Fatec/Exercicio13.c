#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    char tipo;
    float litros, precoPorLitro;

    printf("A - Álcool\nG - Gasolina\nDigite o tipo de combustível: ");
    scanf("%c", &tipo);

    printf("Digite a quantidade de litros: ");
    scanf("%f", &litros);

    switch (tipo)
    {
    case 'a':
    case 'A':
        if (litros <= 15)
        {
            precoPorLitro = 2;
        }
        else if (litros <= 30)
        {
            precoPorLitro = 1.95;
        }
        else
        {
            precoPorLitro = 1.8;
        }

        printf("O cliente comprou %.2fL de álcool de irá pagar R$%.2f.\n", litros, litros * precoPorLitro);
        break;
    case 'g':
    case 'G':
        if (litros <= 15)
        {
            precoPorLitro = 2.9;
        }
        else if (litros <= 30)
        {
            precoPorLitro = 2.85;
        }
        else
        {
            precoPorLitro = 2.75;
        }

        printf("O cliente comprou %.2fL de gasolina de irá pagar R$%.2f.\n", litros, litros * precoPorLitro);
        break;
    default:
        printf("Área inválida.\n");
    }
}