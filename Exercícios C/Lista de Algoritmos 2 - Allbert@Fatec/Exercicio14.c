#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    char tipo;
    float quantidade, precoTotal;

    printf("A - Abacaxi\nG - Goiaba\nDigite o tipo de fruta: ");
    scanf("%c", &tipo);

    printf("Digite a quantidade em KG de frutas adquirida: ");
    scanf("%f", &quantidade);

    switch (tipo)
    {
    case 'a':
    case 'A':
        if (quantidade <= 2)
        {
            precoTotal = 1.9;
        }
        else if (quantidade <= 5)
        {
            precoTotal = 1.8;
        }
        else
        {
            precoTotal = 1.6;
        }

        printf("O cliente comprou %.1fkg de abacaxi de irá pagar R$%.2f.\n", quantidade, quantidade * precoTotal);
        if (quantidade > 8)
        {
            printf("Mas como o cliente comprou mais de 8kg de frutas, irá receber 10%% de desconto. Novo preço: R$%.2f\n", quantidade * precoTotal * .9);
        }
        break;
    case 'g':
    case 'G':
        if (quantidade <= 2)
        {
            precoTotal = 2.5;
        }
        else if (quantidade <= 5)
        {
            precoTotal = 2.4;
        }
        else
        {
            precoTotal = 2.2;
        }

        printf("O cliente comprou %.1fkg de goiaba de irá pagar R$%.2f.\n", quantidade, quantidade * precoTotal);
        if (quantidade > 8)
        {
            printf("Mas como o cliente comprou mais de 8kg de frutas, irá receber 10%% de desconto. Novo preço: R$%.2f\n", quantidade * precoTotal * .9);
        }
        break;
    default:
        printf("Área inválida.\n");
    }
}