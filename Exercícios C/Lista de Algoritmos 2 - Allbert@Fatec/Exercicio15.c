#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    char tipo;
    float valor, imposto;

    printf("L - Limpeza\nA - Alimentação\nV - Vestuário\nDigite o tipo do produto: ");
    scanf("%c", &tipo);

    printf("Digite o valor do produto: ");
    scanf("%f", &valor);

    switch (tipo)
    {
    case 'l':
    case 'L':
        if (valor < 100)
        {
            imposto = .05;
        }
        else if (valor <= 500)
        {
            imposto = .04;
        }
        else
        {
            imposto = .02;
        }

        printf("R$%.2f de produtos de limpeza irá sofrer %.0f%% de imposto e irá custar R$%.2f.\n", valor, imposto * 100, valor + valor * imposto);
        break;
    case 'a':
    case 'A':
        if (valor < 100)
        {
            imposto = .03;
        }
        else if (valor <= 500)
        {
            imposto = .02;
        }
        else
        {
            imposto = .01;
        }

        printf("R$%.2f de alimento irá sofrer %.0f%% de imposto e irá custar R$%.2f.\n", valor, imposto * 100, valor + valor * imposto);
        break;
    case 'v':
    case 'V':
        if (valor < 100)
        {
            imposto = .07;
        }
        else if (valor <= 500)
        {
            imposto = .06;
        }
        else
        {
            imposto = .04;
        }

        printf("R$%.2f de produto do vestuário irá sofrer %.0f%% de imposto e irá custar R$%.2f.\n", valor, imposto * 100, valor + valor * imposto);
        break;
    default:
        printf("Tipo de produto inválido!\n");
    }
}