#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int indicador;
    float raio;

    printf("1 - Raio\n2 - Perímetro\nDigite o indicator da operação: ");
    scanf("%i", &indicador);

    printf("Digite o raio da circunferência: ");
    scanf("%f", &raio);

    switch (indicador)
    {
    case 1:
        printf("A área da circunferência é: %.3fm.\n", raio * raio * 3.14159);
        break;
    case 2:
        printf("O perímetro da circunferência é: %.3fm.\n", 6.28318 * raio);
        break;
    default:
        printf("O indicador de operação foi mal fornecido.\n");
    }
}