#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int faltas;
    float nota;
    char conceito;

    printf("Digite a nota do aluno: ");
    scanf("%f", &nota);

    printf("Digite o número de faltas do aluno: ");
    scanf("%i", &faltas);

    if (nota < 4)
    {
        conceito = 'E';
    }
    else if (nota < 5)
    {
        if (faltas <= 10)
        {
            conceito = 'D';
        }
        else
        {
            conceito = 'E';
        }
    }
    else if (nota < 7.5)
    {
        if (faltas <= 10)
        {
            conceito = 'C';
        }
        else
        {
            conceito = 'D';
        }
    }
    else if (nota < 9)
    {
        if (faltas <= 10)
        {
            conceito = 'B';
        }
        else
        {
            conceito = 'C';
        }
    }
    else
    {
        if (faltas <= 10)
        {
            conceito = 'A';
        }
        else
        {
            conceito = 'B';
        }
    }

    printf("Conceito: %c.\n", conceito);
}