#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    char tipo;
    float temperatura;

    printf("Digite o tipo de temperatura: ");
    scanf("%c", &tipo);

    printf("Digite a temperatura: ");
    scanf("%f", &temperatura);

    switch (tipo)
    {
    case 'f':
    case 'F':
        printf("%.2f Fahrenheit para celsius: %.2f°C.\n", temperatura, (temperatura - 32) / 1.8);
        break;
    case 'c':
    case 'C':
        printf("%.2f°C para Fahrenheit: %.2fF.\n", temperatura, 1.8 * temperatura + 32);
        break;
    default:
        printf("Tipo de temperatura inválida.\n");
    }
}