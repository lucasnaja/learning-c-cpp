#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int idade;

    printf("Digite a idade do nadador: ");
    scanf("%i", &idade);

    if (idade < 8)
    {
        printf("Infantil A.\n");
    }
    else if (idade < 11)
    {
        printf("Infantil B.\n");
    }
    else if (idade < 14)
    {
        printf("Infanto juvenil.\n");
    }
    else if (idade < 18)
    {
        printf("juvenil.\n");
    }
    else
    {
        printf("Sênior.\n");
    }
}