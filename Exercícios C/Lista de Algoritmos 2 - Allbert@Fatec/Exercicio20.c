#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int lado1, lado2, lado3, existe;

    printf("Digite o 1° lado do triângulo: ");
    scanf("%i", &lado1);

    printf("Digite o 2° lado do triângulo: ");
    scanf("%i", &lado2);

    printf("Digite o 3° lado do triângulo: ");
    scanf("%i", &lado3);

    existe = lado1 + lado2 > lado3 && lado1 + lado3 > lado2 && lado2 + lado3 > lado1 ? 1 : 0;

    if (existe)
    {
        if (lado1 == lado2 && lado2 == lado3)
        {
            printf("O triângulo é equilátero.\n");
        }
        else if (lado1 != lado2 && lado2 != lado3 && lado1 != lado3)
        {
            printf("O triângulo é escaleno.\n");
        }
        else
        {
            printf("O triângulo é isósceles.\n");
        }
    }
    else
    {
        printf("Não existe!\n");
    }
}