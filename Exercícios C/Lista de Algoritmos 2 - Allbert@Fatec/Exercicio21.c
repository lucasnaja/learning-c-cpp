#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int horas;
    float valorHora, salarioLiquido, salarioBruto, INSS, IRPF;

    printf("Digite as horas trabalhadas: ");
    scanf("%i", &horas);

    printf("Digite o valor da hora trabalhada: ");
    scanf("%f", &valorHora);

    salarioBruto = horas * valorHora;
    INSS = salarioBruto;

    if (INSS < 868.3f)
    {
        INSS *= .0765;
    }
    else if (INSS < 1447.15f)
    {
        INSS *= .08;
    }
    else if (INSS <= 2894.28f)
    {
        INSS *= .09;
    }
    else
    {
        INSS *= .11;
    }

    IRPF = salarioBruto - INSS;

    if (IRPF > 1499.15f)
    {
        if (IRPF < 3743.19f)
        {
            IRPF *= .15;
            IRPF -= 224.87;
        }
        else
        {
            IRPF *= 0.275;
            IRPF -= 561.02;
        }
    }

    salarioLiquido = salarioBruto - (INSS + IRPF);

    printf("Salário líquido: R$%.2f\nINSS: R$%.2f\nIRPF: R$%.2f\n", salarioLiquido, INSS, IRPF);
}