#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int i = 0, sum = 0;
    while (i < 5)
    {
        int valor;
        printf("Digite o valor: ");
        scanf("%i", &valor);

        if (valor < 0)
        {
            sum++;
        }

        i++;
    }

    printf("Há %i valores negativos.\n", sum);
}