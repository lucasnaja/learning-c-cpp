#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int i = 0, soma = 0;
    while (i < 5)
    {
        int valor;
        printf("Digite o valor: ");
        scanf("%i", &valor);

        if (valor % 3 == 0)
        {
            soma++;
        }

        i++;
    }

    printf("Há %i valores divisíveis por 3.\n", soma);
}