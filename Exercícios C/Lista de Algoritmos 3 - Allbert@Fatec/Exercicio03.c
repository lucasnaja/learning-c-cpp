#include "stdio.h"
#include "locale.h"

void main()
{
    int soma = 0, num, qtd = 0;
    
    while (num != 0)
    {
        printf("Digite o número: ");
        scanf("%i", &num);

        if (num % 2 == 0)
        {
            qtd++;
            soma += num;
        }
    }

    printf("Média dos números pares digitados: %.2f.\n", (float)soma / --qtd);
}