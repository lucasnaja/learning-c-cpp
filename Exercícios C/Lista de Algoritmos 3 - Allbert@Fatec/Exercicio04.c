#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int somaNegativos = 0, i = -1;

    do
    {
        printf("Digite números negativos: ");
        scanf("%i", &i);

        if (i < 0)
        {
            somaNegativos += i;
        }
    } while (i != 0);

    printf("Somatória dos números negativos: %i\n", somaNegativos);
}