#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int num, count = 1;

    printf("Digite um número: ");
    scanf("%i", &num);

    do
    {
        printf("%i x %i = %i\n", num, count, num * count);
        count++;
    } while (count <= 10);
}