#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int codigo, totalVotos = 0, votosCandidato1 = 0, votosCandidato2 = 0, votosNulo = 0, votosBranco = 0;

    printf("1 - Candidato 1\n2 - Candidato 2\n3 - Voto Nulo\n4 - Voto em Branco\n");

    do
    {
        printf("Digite o código: ");
        scanf("%i", &codigo);

        switch (codigo)
        {
        case 0:
            break;
        case 1:
            totalVotos++;
            votosCandidato1++;
            break;
        case 2:
            totalVotos++;
            votosCandidato2++;
            break;
        case 3:
            totalVotos++;
            votosNulo++;
            break;
        case 4:
            totalVotos++;
            votosBranco++;
            break;
        default:
            printf("Esse voto não existe! Tente novamente.\n");
        }
    } while (codigo != 0);

    printf("Porcentagem de votos do Candidato 1: %.2f%%\n", (float)votosCandidato1 / totalVotos * 100);
    printf("Porcentagem de votos do Candidato 2: %.2f%%\n", (float)votosCandidato2 / totalVotos * 100);
    printf("Porcentagem de votos nulos: %.2f%%\n", (float)votosNulo / totalVotos * 100);
    printf("Porcentagem de votos brancos: %.2f%%\n", (float)votosBranco / totalVotos * 100);
}