#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brasil");
    int maiorValor = 0, menorValor = __INT32_MAX__, somaValores = 0, i = 0;

    do
    {
        int valor;
        printf("Digite um valor inteiro e positivo: ");
        scanf("%i", &valor);

        somaValores += valor;

        if (maiorValor < valor)
        {
            maiorValor = valor;
        }

        if (menorValor > valor)
        {
            menorValor = valor;
        }

        i++;
    } while (i < 500);

    printf("Maior valor digitado: %i\n", maiorValor);
    printf("Menor valor digitado: %i\n", menorValor);
    printf("Média dos valores digitados: %.2f\n", somaValores / 500.0f);
}