#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    float areaTotal = 0;
    int continuar;

    do
    {
        char comodo[256];
        float larg, comp, area;
        printf("Digite o cômodo que você irá calcular: ");
        scanf("%s", comodo);

        printf("Digite a largura do(a) %s: ", comodo);
        scanf("%f", &larg);

        printf("Digite o comprimento do(a) %s: ", comodo);
        scanf("%f", &comp);

        area = larg * comp;
        printf("A área do(a) %s é: %.2fm²\n", comodo, area);
        areaTotal += area;

        printf("Deseja continuar calculando? [0, 1] ");
        scanf("%i", &continuar);
    } while (continuar != 0);

    printf("A área total da casa é: %.2fm²\n", areaTotal);
}