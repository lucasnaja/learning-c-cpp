#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int i = 1;

    while (i < 10)
    {
        if (i % 2 == 1)
        {
            int num = i, fatorial = 1;
            
            while (num > 1)
            {
                fatorial *= num;
                num--;
            }

            printf("O Fatorial de %i é %i.\n", i, fatorial);
        }
        i++;
    }
}