#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int i;

    printf("Digite a quantidade de números a serem lidos: ");
    scanf("%i", &i);

    while (i > 0)
    {
        int num, numero, fatorial = 1;
        printf("Digite um número: ");
        scanf("%i", &num);
        numero = num;

        while (num > 1)
        {
            fatorial *= num;
            num--;
        }

        printf("O Fatorial de %i é %i.\n", numero, fatorial);

        i--;
    }
}