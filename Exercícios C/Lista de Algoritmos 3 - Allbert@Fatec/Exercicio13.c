#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int a = 1, b = 1, c = 2, i = 0;
    // 0 1 1 2 3
    printf("1, 1, ");

    do
    {
        printf("%i, ", c);
        a = b;
        b = c;
        c = a + b;
        i++;
    } while (i < 15);
    printf("\n");
}