#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int i = 1;

    do
    {
        int num = 2, somaDivisores = 1;

        do
        {
            if (i % num == 0)
            {
                somaDivisores += num;
            }
            num++;
        } while (num <= i / 2);

        if (i == somaDivisores) {
            printf("%i é um número perfeito.\n", i);
        }

        i++;
    } while (i <= 800);
}