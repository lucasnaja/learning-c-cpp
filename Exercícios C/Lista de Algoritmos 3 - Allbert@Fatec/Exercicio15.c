#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int i = 92;

    do
    {
        int num = 2, qtdDiv = 2;

        do
        {
            if (i % num == 0)
            {
                qtdDiv++;
                break;
            }

            num++;
        } while (num < i - 1);

        if (qtdDiv == 2)
        {
            printf("O número %i é primo!\n", i);
        }

        i++;
    } while (i < 1478);
}