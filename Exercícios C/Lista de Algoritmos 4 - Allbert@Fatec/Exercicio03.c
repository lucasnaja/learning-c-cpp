#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   for (int i = 2; i <= 100; i++)
   {
      if (i % 2 == 0)
      {
         printf("%i\n", i);
      }
   }
}