#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   for (int i = 100; i <= 200; i++)
   {
      if (i % 2 == 1)
      {
         printf("%i\n", i);
      }
   }
}