#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   for (int i = 1; i <= 20; i++)
   {
      printf("%i\n", i * i);
   }
}