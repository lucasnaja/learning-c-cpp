#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   for (int i = 5; i <= 500; i++)
   {
      if (i % 5 == 0)
      {
         printf("%i\n", i);
      }
   }
}