#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   for (int i = 0; i < 10; i++)
   {
      int numero;
      printf("Digite um número: ");
      scanf("%i", &numero);

      printf("%i\n", numero * numero);
   }
}