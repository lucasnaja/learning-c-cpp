#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   int soma = 0;
   for (int i = 1; i <= 20; i++)
   {
      printf("Número: %i\n", i);
      soma += i * i;
   }

   printf("Soma do quadrado deles: %i\n", soma);
}