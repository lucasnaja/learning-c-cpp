#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   float soma = 0;
   for (int i = 1; i <= 20; i++)
   {
      printf("Número: %i\n", i);
      soma += i / 2.0;
   }

   printf("Soma da metade deles: %.2f\n", soma);
}