#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   int menor = __INT32_MAX__, maior = 0;
   for (int i = 0; i < 10; i++)
   {
      int num;
      printf("Digite o número: ");
      scanf("%i", &num);

      if (num > maior)
      {
         maior = num;
      }
      if (num < menor)
      {
         menor = num;
      }
   }

   printf("Menor número: %i\nMaior número: %i\n", menor, maior);
}