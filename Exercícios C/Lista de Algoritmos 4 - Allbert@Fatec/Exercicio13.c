#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   int num;
   printf("Digite o número: ");
   scanf("%i", &num);

   for (int i = 1; i <= num; i++)
   {
      if (i % 3 == 0 && i % 5 == 0) {
         printf("%i é múltiplo de 3 e 5, ao mesmo tempo.\n", i);
      }
   }
}