#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   int soma = 0;

   for (int i = 1; i <= 10; i++)
   {
      int num;
      printf("Digite o número: ");
      scanf("%i", &num);

      if (num > 30)
      {
         soma++;
      }
   }

   printf("Foram digitados %i números maiores que 30.\n", soma);
}