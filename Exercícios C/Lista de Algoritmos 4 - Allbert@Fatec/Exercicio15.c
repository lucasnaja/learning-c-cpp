#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   float soma = 1;
   int num;

   printf("Digite o número: ");
   scanf("%i", &num);

   for (int i = 2; i <= num; i++)
   {
      soma += 1.0 / i;
   }

   printf("H = %.2f\n", soma);
}