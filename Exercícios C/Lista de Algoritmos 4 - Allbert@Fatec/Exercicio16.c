#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   float media, porcentualNegativos, porcentualPositivos;
   int qtdPositivos = 0, somaPositivos = 0, qtdNegativos = 0, somaNegativos = 0, qtdTotal = 0, soma = 0;

   for (int i = 0; i < 20; i++)
   {
      int num;
      printf("Digite o %i° número: ", i + 1);
      scanf("%i", &num);

      if (num > 0)
      {
         qtdPositivos++;
         somaPositivos += num;
      }
      else
      {
         qtdNegativos++;
         somaNegativos += num;
      }

      soma += num;
      qtdTotal++;
   }

   media = (float)soma / qtdTotal;
   porcentualPositivos = (float)somaPositivos / soma;
   porcentualNegativos = (float)somaNegativos / soma;

   printf("Média aritmética: %.2f\n", media);
   printf("Números positivos: %i\n", qtdPositivos);
   printf("Números negativos: %i\n", qtdNegativos);
   printf("Média dos positivos: %.0f%%\n", porcentualPositivos * 100);
   printf("Média dos negativos: %.0f%%\n", porcentualNegativos * 100);
}