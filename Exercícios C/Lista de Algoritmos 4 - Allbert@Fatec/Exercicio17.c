#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   for (int i = 0; i < 50; i++)
   {
      float media, soma = 0;

      for (int j = 0; j < 3; j++)
      {
         int nota;
         printf("Digite a nota da %i° prova: ", j + 1);
         scanf("%i", &nota);

         soma += nota;
      }

      media = soma / 3;
      printf("A média final do aluno é: %.2f\n", media);
   }
}