#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   for (int i = 0; i < 15; i++)
   {
      int a, b;

      printf("Digite o valor A: ");
      scanf("%i", &a);

      printf("Digite o valor B: ");
      scanf("%i", &b);

      for (int i = a; i <= b; i++)
      {
         printf("%i, ", i);
      }

      printf("\n");
   }
}