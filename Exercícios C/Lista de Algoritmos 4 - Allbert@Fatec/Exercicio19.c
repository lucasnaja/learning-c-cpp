#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   int somaIdade = 0, somaSexoMasc = 0, somaSexoFem = 0, qtdSalFem500 = 0;
   float mediaSalario, somaSalario = 0, mediaIdade, porcentualMasculino, porcentualFeminino;

   for (int i = 0; i < 40; i++)
   {
      char sexo;
      int idade;
      float salario;

      printf("Digite o sexo do %i° cidadão: ", i + 1);
      scanf("%s", &sexo);

      printf("Digite a idade do %i° cidadão: ", i + 1);
      scanf("%i", &idade);

      printf("Digite o salário do %i cidadão: ", i + 1);
      scanf("%f", &salario);

      somaSalario += salario;
      somaIdade += idade;

      switch (sexo)
      {
      case 'm':
      case 'M':
         somaSexoMasc += idade;
         break;
      case 'f':
      case 'F':
         somaSexoFem += idade;
         if (salario < 500)
         {
            qtdSalFem500++;
         }
      }
   }

   mediaSalario = somaSalario / 3;
   mediaIdade = somaIdade / 3.0;
   porcentualMasculino = (float)somaSexoMasc / somaIdade;
   porcentualFeminino = (float)somaSexoFem / somaIdade;

   printf("Média de salário do grupo: %.2f\n", mediaSalario);
   printf("Média de idade do grupo: %.2f\n", mediaIdade);
   printf("Porcentual de habitantes do sexo masculino: %.2f%%\n", porcentualMasculino * 100);
   printf("Porcentual de habitantes do sexo feminino: %.2f%%\n", porcentualFeminino * 100);
   printf("Quantidade de mulheres com salário inferior a 500R$: %i\n", qtdSalFem500);
}