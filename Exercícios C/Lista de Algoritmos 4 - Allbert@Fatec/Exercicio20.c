#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   int somaPositivos = 0, somaNegativos = 0, somaMasculino = 0, somaFeminino = 0, qtdMasculinoOtimo = 0, qtdPessoas = 2;
   float porcentagemPositivo, porcentagemNegativo, porcentagemMasculino, porcentagemFeminino;

   for (int i = 0; i < qtdPessoas; i++)
   {
      int sexo, opiniao;
      printf("Qual seu sexo? [1 – Masculino e 2 – Feminino]: ");
      scanf("%i", &sexo);

      printf("Qual sua opinião? [1 – Ótimo, 2 – Bom, 3 – Regular, 4 – Ruim]: ");
      scanf("%i", &opiniao);

      switch (sexo)
      {
      case 1:
         somaMasculino++;
         if (opiniao == 1)
         {
            qtdMasculinoOtimo++;
         }
         break;
      case 2:
         somaFeminino++;
      }

      switch (opiniao)
      {
      case 1:
      case 2:
         somaPositivos++;
         break;
      case 3:
      case 4:
         somaNegativos++;
      }
   }

   porcentagemPositivo = (float)somaPositivos / qtdPessoas * 100;
   porcentagemNegativo = (float)somaNegativos / qtdPessoas * 100;
   porcentagemMasculino = (float)somaMasculino / qtdPessoas * 100;
   porcentagemFeminino = (float)somaFeminino / qtdPessoas * 100;

   printf("Porcentagem de ótimos e bom: %.2f%%\n", porcentagemPositivo);
   printf("Porcentagem de regular e ruim: %.2f%%\n", porcentagemNegativo);
   printf("Porcentagem de homens: %.2f%%\n", porcentagemMasculino);
   printf("Porcentagem de mulheres: %.2f%%\n", porcentagemFeminino);
   printf("Quantidade de ótimos pelos homens: %i\n", qtdMasculinoOtimo);
}