#include "stdio.h"
#include "locale.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   int a, b, c;

   printf("Digite o primeiro número: ");
   scanf("%i", &a);

   printf("Digite o segundo número: ");
   scanf("%i", &b);

   printf("%i, %i, ", a, b);
   for (int i = 2; i < 25; i++)
   {
      c = a + b;
      printf("%i, ", c);
      a = b;
      b = c;
   }

   printf("\n");
}