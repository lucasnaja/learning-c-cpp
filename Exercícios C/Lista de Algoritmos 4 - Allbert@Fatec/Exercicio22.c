#include "stdio.h"
#include "locale.h"
#include "stdlib.h"
#include "time.h"

void main()
{
   setlocale(LC_ALL, "Portuguese_Brazil");
   srand(time(NULL));
   int numAnterior, numRepetido, qtdRepetido = 1, qtdRepetidoSecundario = 1;

   for (int i = 0; i <= 30; i++)
   {
      int numAtual = rand() % 5 + 1;
      printf("%i ", numAtual);

      if (i == 0)
      {
         numAnterior = numAtual;
         numRepetido = numAtual;
         continue;
      }

      if (numAtual == numAnterior)
      {
         qtdRepetidoSecundario++;

         if (qtdRepetidoSecundario > qtdRepetido)
         {
            numRepetido = numAtual;
            qtdRepetido = qtdRepetidoSecundario;
         }
      }
      else
      {
         qtdRepetidoSecundario = 1;
      }

      numAnterior = numAtual;
   }

   printf("\nNúmero que mais se repete: %i\n", numRepetido);
   printf("Quantidade de vezes que se repete: %i\n", qtdRepetido);
}