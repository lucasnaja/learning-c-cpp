#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int v[10], i;
    srand(time(0));

    for (i = 0; i < 10; i++)
    {
        v[i] = rand() % 101;
        if (v[i] >= 15 && v[i] <= 45)
        {
            printf("V[%i] = %i\n", i, v[i]);
        }
    }
}