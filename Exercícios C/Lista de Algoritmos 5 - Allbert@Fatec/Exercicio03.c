#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int v[100], i, zTo100 = 0, h1To200 = 0, t1To300 = 0, t1To400 = 0, f1To500 = 0;
    srand(time(0));

    for (i = 0; i < 100; i++)
    {
        v[i] = rand() % 501;
        if (v[i] < 101)
        {
            zTo100++;
        }
        else if (v[i] < 201)
        {
            h1To200++;
        }
        else if (v[i] < 301)
        {
            t1To300++;
        }
        else if (v[i] < 401)
        {
            t1To400++;
        }
        else
        {
            f1To500++;
        }
    }

    printf("Valores de 0 a 100: %i\n", zTo100);
    printf("Valores de 101 a 200: %i\n", h1To200);
    printf("Valores de 201 a 300: %i\n", t1To300);
    printf("Valores de 301 a 400: %i\n", t1To400);
    printf("Valores de 401 a 500: %i\n", f1To500);
}