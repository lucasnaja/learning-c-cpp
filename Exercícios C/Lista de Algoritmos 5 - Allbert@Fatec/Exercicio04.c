#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int v[50], i, maior = 0, menor;
    srand(time(0));

    for (i = 0; i < 50; i++)
    {
        v[i] = rand() % 201;

        if (v[i] > maior)
        {
            maior = v[i];
        }

        if (i == 0)
        {
            menor = v[i];
        }
        else if (v[i] < menor)
        {
            menor = v[i];
        }
    }

    printf("Menor: %i\n", menor);
    printf("Maior: %i\n", maior);
}