#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int a[30], b[30], i;
    srand(time(0));

    for (i = 0; i < 30; i++)
    {
        a[i] = rand() % 150 + 1;

        b[i] = i % 2 == 1 ? a[i] / 2 : a[i] / 3;

        printf("A[%i] = %i. B[%i] = %i\n", i, a[i], i, b[i]);
    }
}