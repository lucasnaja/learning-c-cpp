#include "stdio.h"
#include "stdlib.h"
#include "time.h"

void main()
{
    int a[30], i, qtd = 0, x;
    srand(time(0));

    printf("Digite o X: ");
    scanf("%i", &x);

    for (i = 0; i < 30; i++)
    {
        a[i] = rand() % 150 + 1;

        if (a[i] % 2 == 1 && a[i] > x)
        {
            qtd++;
        }
    }

    printf("Números ímpares maiores que X: %i\n", qtd);
}