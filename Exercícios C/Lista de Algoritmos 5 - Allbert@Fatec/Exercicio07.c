#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int a[50], i, soma = 0, x;
    float media;
    srand(time(0));

    printf("Digite o X: ");
    scanf("%i", &x);

    for (i = 0; i < 50; i++)
    {
        a[i] = rand() % 100 + 1;

        if (a[i] % 2 == 0 && a[i] > x)
        {
            soma += a[i];
        }
    }

    media = (float)soma / 50;

    printf("Média dos números maiores que %i: %f\n", x, media);
}