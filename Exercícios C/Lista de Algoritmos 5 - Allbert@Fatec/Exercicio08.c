#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int a[20], i, j;

    srand(time(0));

    for (i = 0; i < 20; i++)
    {
        a[i] = rand() % 101;

        printf("A[%i] = %i\n", i, a[i]);
    }

    printf("\n");

    for (i = 0, j = 19; i < 10; i++, j--)
    {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    for (i = 0; i < 20; i++)
    {
        printf("A[%i] = %i\n", i, a[i]);
    }
}