#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int a[30], i, qtdI = 0;
    float impares;

    srand(time(0));

    for (i = 0; i < 30; i++)
    {
        a[i] = rand() % 101;

        if (a[i] % 2 == 1)
        {
            qtdI++;
        }
        else
        {
            a[i] = 0;
        }

        printf("A[%i] = %i\n", i, a[i]);
    }

    impares = (float)qtdI / 30 * 100;

    printf("\nPorcentagem de valores ímpares: %.2f%%\n", impares);
}