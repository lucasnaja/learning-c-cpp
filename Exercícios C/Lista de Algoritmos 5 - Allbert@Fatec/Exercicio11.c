#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int a[20], i, n, qtdM = 0;
    float impares;

    printf("Digite o N: ");
    scanf("%i", &n);

    srand(time(0));

    for (i = 0; i < 20; i++)
    {
        a[i] = rand() % 101 + 100;

        if (a[i] > n)
        {
            qtdM++;
        }
        else if (a[i] < n)
        {
            a[i] = 0;
        }

        printf("A[%i] = %i\n", i, a[i]);
    }

    printf("Valores maiores que N: %i\n", qtdM);
}