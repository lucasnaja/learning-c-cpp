#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    int qtdPao, qtdBroa;
    float precoTotal;

    printf("Digite a quantidade de pãos vendidos: ");
    scanf("%i", &qtdPao);

    printf("Digite a quantidade de broas vendidas: ");
    scanf("%i", &qtdBroa);

    precoTotal = qtdPao * .12f + qtdBroa * 1.5f;
    printf("Total: R$%.2f\nPoupança de 10%%: R$%.2f\n", precoTotal, precoTotal * .1);
}