#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    float preco;

    printf("Gasto médo do cliente: ");
    scanf("%f", &preco);

    if (preco <= 500)
    {
        printf("Cliente comum.\n");
    }
    else if (preco <= 1500)
    {
        printf("Cliente preferencial.\n");
    }
    else
    {
        printf("Cliente exclusivo.\n");
    }
}