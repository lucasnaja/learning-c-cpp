#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    char codigoSuco;
    int quantidade;
    float precoUnidade;

    printf("Suco Açaí - A\nSuco Beterraba: B\nSuco Cana: C\n");

    printf("Digite o código do suco: ");
    scanf("%c", &codigoSuco);

    printf("Digite a quantidade: ");
    scanf("%i", &quantidade);

    switch (codigoSuco)
    {
    case 'a':
    case 'A':
        if (quantidade <= 5)
        {
            precoUnidade = 3;
        }
        else if (quantidade <= 15)
        {
            precoUnidade = 2.6f;
        }
        else
        {
            precoUnidade = 2.1f;
        }
        printf("Valor a pagar: R$%.2f\n", quantidade * precoUnidade);
        break;
    case 'b':
    case 'B':
        if (quantidade <= 5)
        {
            precoUnidade = 4;
        }
        else if (quantidade <= 15)
        {
            precoUnidade = 3.5f;
        }
        else
        {
            precoUnidade = 3;
        }
        printf("Valor a pagar: R$%.2f\n", quantidade * precoUnidade);
        break;
    case 'c':
    case 'C':
        if (quantidade <= 5)
        {
            precoUnidade = 5;
        }
        else if (quantidade <= 15)
        {
            precoUnidade = 4.3f;
        }
        else
        {
            precoUnidade = 3.7f;
        }
        printf("Valor a pagar: R$%.2f\n", quantidade * precoUnidade);
        break;
    default:
        printf("Código de suco inválido.\n");
    }
}