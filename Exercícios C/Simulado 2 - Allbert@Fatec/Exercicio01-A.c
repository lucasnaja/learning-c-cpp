#include "stdio.h"
#include "locale.h"

void main() {
   setlocale(LC_ALL, "Portuguese_Brazil");
   float peso;
   int tempo = 0;

   printf("Digite o peso da peça: ");
   scanf("%f", &peso);

   while (peso < 100) {
      peso += peso * .13;
      tempo++;
   }

   printf("Tempo total: %i\n", tempo);
}