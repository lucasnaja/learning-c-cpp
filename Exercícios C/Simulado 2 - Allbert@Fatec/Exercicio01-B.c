#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    float massa;
    int minutos = 0;

    printf("Digite a massa do material [KG]: ");
    scanf("%f", &massa);

    while (massa > 1)
    {
        massa -= massa * .08;
        minutos++;
    }

    printf("Quantidade de minutos: %i\n", minutos);
}