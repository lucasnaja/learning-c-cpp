#include "stdio.h"
#include "locale.h"

void main() {
   setlocale(LC_ALL, "Portuguese_Brazil");
   int qtdPessoas = 60, menorIdade, qtdTorcedoresSexoMasculino = 0, qtdCor = 0, qtdPal = 0, qtdSP = 0, qtdSantos = 0, torcedoresSantosIdade = 0;
   float porcentagemCor, porcentagemPal, porcentagemSP, porcentagemSantos, mediaSantos;

   for (int i = 0; i < qtdPessoas; i++) {
      int idade = 0, timeFavorito;
      char sexo;

      printf("Sexo? [M, F] ");
      scanf("%s", &sexo);

      printf("Time favorito? [1 – Corinthians | 2 – Palmeiras | 3 – São Paulo | 4 - Santos] ");
      scanf("%i", &timeFavorito);

      printf("Qual sua idade? ");
      scanf("%i", &idade);

      if (i == 0) {
         menorIdade = idade;
      } else if (idade < menorIdade) {
         menorIdade = idade;
      }

      switch (timeFavorito) {
         case 1:
            qtdCor++;
            break;
         case 2:
            qtdPal++;
            break;
         case 3:
            qtdSP++;
            break;
         case 4:
            torcedoresSantosIdade += idade;
            qtdSantos++;
      }

      switch (sexo) {
         case 'm':
         case 'M':
            qtdTorcedoresSexoMasculino++;
      }
   }

   porcentagemCor = (float)qtdCor / qtdPessoas;
   porcentagemPal = (float)qtdPal / qtdPessoas;
   porcentagemSP = (float)qtdSP / qtdPessoas;
   porcentagemSantos = (float)qtdSantos / qtdPessoas;
   mediaSantos = (float)torcedoresSantosIdade / qtdSantos;

   printf("Porcentagem de corinthianos: %.2f%%\n", porcentagemCor * 100);
   printf("Porcentagem de palmeirenses: %.2f%%\n", porcentagemPal * 100);
   printf("Porcentagem de são paulinos: %.2f%%\n", porcentagemSP * 100);
   printf("Porcentagem de santistas: %.2f%%\n", porcentagemSantos * 100);
   printf("Média de idade dos torcedores de Santos: %.2f\n", mediaSantos);
   printf("Menor idade: %i\n", menorIdade);
   printf("Quantidade de torcedores do sexo masculino: %i\n", qtdTorcedoresSexoMasculino);
}