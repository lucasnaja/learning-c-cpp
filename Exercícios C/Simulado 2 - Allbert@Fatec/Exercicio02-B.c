#include "stdio.h"
#include "locale.h"

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    float pF, pV, pT, pO, mediaI;
    int qpF = 0, qpV = 0, qpT = 0, qpO = 0, qtdPLPri = 0, qPLP = 0, siLP = 0, maiorI = 0, qtdPessoas = 2;

    for (int i = 0; i < qtdPessoas; i++)
    {
        int eF, pP, id;

        printf("Qual seu esporte favorito? [1 – Futebol | 2 – Vôlei | 3 – Tênis | 4 – Outros] ");
        scanf("%i", &eF);

        printf("Onde prefere praticá-los? [1 – Locais públicos | 2 – Locais privados] ");
        scanf("%i", &pP);

        printf("Qual sua idade? ");
        scanf("%i", &id);

        if (maiorI < id)
        {
            maiorI = id;
        }

        switch (eF)
        {
        case 1:
            qpF++;
            break;
        case 2:
            qpV++;
            break;
        case 3:
            qpT++;
            break;
        case 4:
            qpO++;
        }

        switch (pP)
        {
        case 1:
            qPLP++;
            break;
        case 2:
            siLP += id;
            qtdPLPri++;
        }
    }

    pF = (float)qpF / qtdPessoas * 100;
    pV = (float)qpV / qtdPessoas * 100;
    pT = (float)qpT / qtdPessoas * 100;
    pO = (float)qpO / qtdPessoas * 100;
    mediaI = (float)siLP / qtdPLPri;

    printf("Futebol: %.2f%%\n", pF);
    printf("Vôlei: %.2f%%\n", pV);
    printf("Tênis: %.2f%%\n", pT);
    printf("Outros: %.2f%%\n", pO);
    printf("Média idade locais privados: %.2f\n", mediaI);
    printf("Maior idade: %i\n", maiorI);
    printf("Qtd de pessoas locais públicos: %i\n", qPLP);
}