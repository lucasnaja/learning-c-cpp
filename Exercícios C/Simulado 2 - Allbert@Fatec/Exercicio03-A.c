#include "stdio.h"
#include "locale.h"

void main() {
   setlocale(LC_ALL, "Portuguese_Brazil");
   int num, operacao;

   printf("Digie um número: ");
   scanf("%i", &num);

   printf("Digite uma operação:\n1 – Informar se o número é par ou ímpar\n2 – Informar os divisores de um número\n3 – Escrever a sequência de 1 até número digitado pelo usuário\n: ");
   scanf("%i", &operacao);

   switch (operacao) {
      case 1:
         if (num % 2 == 0) {
            printf("Número é par!\n");
         } else {
            printf("Número é ímpar!\n");
         }
         break;
      case 2:
         printf("Divisores: ");
         for (int i = 1; i <= num; i++) {
            if (i == num) {
               printf("e %i\n", num);
            } else if (num % i == 0) {
               printf("%i, ", i);
            }
         }
         break;
      case 3:
         printf("Sequência: ");
         for (int i = 1; i <= num; i++) {
            printf("%i ", i);
         }
         printf("\n");
         break;
      default:
         printf("Operação inválida.\n");
   }
}