#include "stdio.h"
#include "locale.h"

int fact(int);
int prime(int);

void main()
{
    setlocale(LC_ALL, "Portuguese_Brazil");
    // Desenvolver um programa que leia o valor de um número inteiro e a
    // operação desejada; calcule, então, a resposta adequada. Utilize os códigos
    // a seguir para ler qual a operação escolhida.
    // 1 – Calcular fatorial;
    //     2 – Informar se o número é primo;
    // 3 – A soma da sequência de 1 até o valor digitado pelo usuário;
    int num, operacao, soma = 0;

    printf("Digite um número: ");
    scanf("%i", &num);

    printf("1 - Calcular fatorial\n2 - Informar se o número é primo\n3 - A soma da sequência de 1 até o valor digitado pelo usuário\nDigite um número: ");
    scanf("%i", &operacao);

    switch (operacao)
    {
    case 1:
        printf("Fatorial de %i: %i\n", num, fact(num));
        break;
    case 2:
        if (prime(num))
        {
            printf("O número %i é primo!\n", num);
        }
        else
        {
            printf("O número %i não é primo!\n", num);
        }
        break;
    case 3:
        for (int i = 1; i <= num; i++)
        {
            soma += i;
        }

        printf("A soma de 1 até %i é: %i\n", num, soma);
        break;
    default:
        printf("Opção inválida.\n");
    }
}

int fact(int num)
{
    int fact = 1;

    for (int i = num; i > 0; i--)
    {
        fact *= i;
    }

    return fact;
}

int prime(int num)
{
    int qtd = 2;

    for (int i = 2; i < num; i++)
    {
        if (num % i == 0)
        {
            qtd++;
            break;
        }
    }

    if (qtd != 2 || num <= 1)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}